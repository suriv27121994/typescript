//type Y = "Y";
//const testing: Y = "X"; // Type '"Y"' is not assignable to type '"X"'

// Use this in below //

const X = "X"; // typeof X: "X"
type Y = string;
const testing: Y = X; // running

console.log(testing);

