//const obj = { x: "this is x", y: "this is y" }; // typeof obj: { x: string; y: string; }
//obj.z = 'z'; // Property 'z' does not exist on type '{ x: string; y: string; }'

// --- //

// we can use a type annotation to also support other string keys than "a" and "b"
const obj: { [Key: string]: string } = { x: "this is x", y: "this is y" };
obj.z = "z"; // running

console.log(obj);

