//const arr = [10, "100", null]; // typeof array: (number | string | null)[]
//const item = arr[1]; // typeof item: number | string | null
//arr.push(true); // the argument of type 'true' is not assignable to parameter of type 'number | string | null'

// ---

// we can use a type annotation to also support "boolean" values
const arr: (number | string | null | boolean)[] = [10, "100", null];
arr.push(true); // ok

console.log(arr);
