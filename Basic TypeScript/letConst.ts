const thisConstStr = "testing"; // type: "testing" Str = String
let thisLetStr = "testing"; // type: string

const aConstNmbr = 100; // type: 100, Nmbr = Number
let aLetNmbr = 100; // type: number

//take a string
const takeStr = (x: string) => x;
const resultStr = takeStr(thisConstStr); // typeof result: string

//take a number
const takeNmbr = (y: Number) => y;
const resultNmbr = takeNmbr(aConstNmbr); // typeof result: number

console.log("This is a string result:", resultStr);
console.log("This is a number result:", resultNmbr);
